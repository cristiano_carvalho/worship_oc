source 'https://rubygems.org'

ruby '2.2.4'
gem 'rails', '4.2.6'

# Authentication
gem 'devise'
gem 'devise_invitable'
gem 'pundit'

# Image upload
gem 'paperclip'

# Data
gem 'pg'

# Javascript
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'turbolinks'
gem 'coffee-rails', '~> 4.1.0'

# Assets
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'autoprefixer-rails'

# Design
gem 'bootstrap_form'
gem 'bootstrap-sass'
gem 'font-awesome-rails'

group :development do
  # Server
  gem 'thin'

  # Docs
  gem 'sdoc', '~> 0.4.0', group: :doc

  # Errors
  gem 'better_errors'
  gem 'binding_of_caller'

  # Debugging
  gem 'web-console', '~> 2.0'

  # Automated Tasks
  gem 'guard-rspec'
  gem 'guard-rails'
  gem 'guard-bundler'
  gem 'guard-livereload'
  gem 'guard-migrate'
  gem 'rack-livereload'
end

group :development, :test do
  # Preloader
  gem 'spring'
  gem 'spring-commands-rspec'

  # Debugging
  gem 'pry-rails'
  gem 'byebug'
  gem 'awesome_print'

  # Testing
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'capybara'
  gem 'poltergeist'
  gem 'launchy'
end

group :test do
  # Helpers
  gem 'json-schema-rspec'
  gem 'shoulda-matchers', github: 'thoughtbot/shoulda-matchers'
  gem 'database_cleaner'

  # Code analysis
  gem 'rubocop', require: false

  # Coverage
  gem 'simplecov', require: false
end

group :production do
  # Server
  gem 'puma'

  # Assets
  gem 'rails_12factor'
end

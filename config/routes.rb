Rails.application.routes.draw do
  devise_for :users, prefix: 'worship_'

  resources :dashboard, only: [:index]
  resources :users
  resources :birthdays, only: [:index]
  resources :songs
  resources :hymns
  resources :events

  devise_scope :user do
    authenticated :user do
      root to: 'dashboard#index', as: 'authenticated'
    end

    unauthenticated :user do
      root to: 'devise/sessions#new', as: 'unauthenticated'
    end
  end

  root 'dashboard#index'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :songs, only: [:index, :show, :create, :update, :destroy]
    end
  end
end

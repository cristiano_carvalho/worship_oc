require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  before(:each) do
    ActionMailer::Base.delivery_method = :test
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @user = build_stubbed(:user, role: :admin)
    UserMailer.welcome_email(@user).deliver_now
  end

  after(:each) do
    ActionMailer::Base.deliveries.clear
  end

  describe 'Send welcome email after register an user' do
    it 'it sends an email' do
      expect(ActionMailer::Base.deliveries.count).to eql(1)
    end

    it 'renders the receiver email' do
      expect(ActionMailer::Base.deliveries.first).to have_text(@user.email)
    end

    it 'should set the subject to the correct subject' do
      expect(ActionMailer::Base.deliveries.first.subject).to have_text('Bem vindo ao Adoração O.C.!')
    end

    it 'renders the sender email' do
      expect(ActionMailer::Base.deliveries.first.from).to have_text('naoresponda@adoracaooc.com')
    end
  end
end

require 'rails_helper'

RSpec.describe HymnsController, type: :controller do
  let(:user) { create(:user, role: :user) }

  before :each do
    login_with(user)
  end

  describe 'GET #index' do
    context 'show only hymns with current week number' do
      let(:hymns) { create_list(:hymn, 5) }

      it 'assign hymns to @hymns' do
        get :index
        expect(assigns[:hymns]).to match_array(hymns)
      end

      it 'render index template' do
        get :index
        expect(response).to render_template(:index)
      end

      it 'return http status ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end

    context 'not show hymns out of current week number' do
      let(:hymns) { create_list(:hymn, 5, week_number: Date.current.cweek - 1) }

      it 'not assign hymns to @hymns' do
        get :index
        expect(assigns[:hymns]).to_not match_array(hymns)
      end

      it 'render index template' do
        get :index
        expect(response).to render_template(:index)
      end

      it 'return http status ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET #show' do
    context 'when hymn exist' do
      let(:hymn) { create(:hymn) }

      it 'assigns loaded hymn to @hymn' do
        get :show, id: hymn.id
        expect(assigns[:hymn]).to eq(hymn)
      end

      it 'render template show' do
        get :show, id: hymn.id
        expect(response).to have_http_status(:ok)
      end

      it 'return http status ok' do
        get :show, id: hymn.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when hymn does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :show, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'GET #new' do
    it 'assigns a new Hymn to @hymn' do
      get :new
      expect(assigns[:hymn]).to be_a_new(Hymn)
    end

    it 'render template new' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'return http status ok' do
      get :new
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #edit' do
    context 'when hymn exists' do
      let(:hymn) { create(:hymn, user_id: user.id) }

      before :each do
        login_with(user)
      end

      it 'assigns loaded hymn to @hymn' do
        get :edit, id: hymn.id
        expect(assigns[:hymn]).to eq(hymn)
      end

      it 'render template edit' do
        get :edit, id: hymn.id
        expect(response).to render_template(:edit)
      end

      it 'return http status ok' do
        get :edit, id: hymn.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when hymn does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      let(:hymn) { attributes_for(:hymn) }

      it 'create hymn' do
        expect do
          post :create, hymn: hymn
        end.to change(Hymn, :count).by(1)
      end

      it 'redirect to event details (show)' do
        post :create, hymn: hymn
        expect(response).to redirect_to(hymm_path(Hymn.last))
      end

      it 'return an http status redirect' do
        post :create, hymn: hymn
        expect(response).to have_http_status(:redirect)
      end
    end

    context 'with invalid params' do
      let(:invalid_hymn) { attributes_for(:hymn, title: nil) }

      it 'do not create hymn' do
        expect do
          post :create, hymn: invalid_hymn
        end.to_not change(Hymn, :count)
      end

      it 'render new template' do
        post :create, hymn: invalid_hymn
        expect(response).to render_template(:new)
      end

      it 'returns an http status ok' do
        post :create, hymn: invalid_hymn
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'PUT #update' do
    context 'when hymn exist' do
      context 'with valid params' do
        let(:hymn) { create(:hymn, user_id: user.id) }
        let(:attributes) { attributes_for(:hymn) }

        it 'update hymn attributes' do
          put :update, id: hymn.id, hymn: attributes
          hymn.reload
          expect(hymn.title).to eq(attributes[:title])
        end

        it 'redirect to hymn details (show)' do
          put :update, id: hymn.id, hymn: attributes
          expect(response).to redirect_to(hymm_path(hymn))
        end

        it 'have http status redirect' do
          put :update, id: hymn.id, hymn: attributes
          expect(response).to have_http_status(:redirect)
        end
      end

      context 'with invalid params' do
        let(:hymn) { create(:hymn, user_id: user.id) }
        let(:invalid_attributes) { attributes_for(:hymn, title: nil) }

        it 'not update hymn attributes' do
          expect do
            put :update, id: hymn.id, hymn: invalid_attributes
          end.to_not change(hymn, :title)
        end

        it 'render edit view' do
          put :update, id: hymn.id, hymn: invalid_attributes
          expect(response).to render_template(:edit)
        end

        it 'have http status ok' do
          put :update, id: hymn.id, hymn: invalid_attributes
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'when hymn not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when hymn exist' do
      let(:hymn) { create(:hymn, user_id: user.id) }

      it 'remove hymn from database' do
        hymn
        expect do
          delete :destroy, id: hymn.id
        end.to change(Hymn, :count).by(-1)
      end

      it 'redirect to hymns listing' do
        delete :destroy, id: hymn.id
        expect(response).to redirect_to(hymns_path)
      end
    end

    context 'when hymn not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          delete :destroy, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

end

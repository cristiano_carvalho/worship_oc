require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:user) { build_stubbed(:user, role: :user) }
  let(:admin) { build_stubbed(:user, role: :admin) }

  describe 'GET #index' do
    before :each do
      login_with(user)
    end

    let(:users) { create_list(:user, 5) }

    it 'assign users to @users' do
      get :index
      expect(assigns[:users]).to match_array(users)
    end

    it 'render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'return http status ok' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #new' do
    before :each do
      login_with(admin)
    end

    it 'assign a new User to @user' do
      get :new
      expect(assigns[:user]).to be_a_new(User)
    end

    it 'render template new' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'return http status ok' do
      get :new
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    before :each do
      login_with(admin)
    end

    context 'with valid params' do
      let(:user) { attributes_for(:user, role: :user) }

      it 'create user' do
        expect do
          post :create, user: user
        end.to change(User, :count).by(1)
      end

      it 'redirect to users detail (show)' do
        post :create, user: user
        expect(response).to redirect_to(user_path(User.last))
      end

      it 'return http status redirect' do
        post :create, user: user
        expect(response).to have_http_status(:redirect)
      end
    end

    context 'with invalid params' do
      let(:invalid_user) { attributes_for(:user, email: nil, role: :user) }

      it 'do not create user' do
        expect do
          post :create, user: invalid_user
        end.to_not change(User, :count)
      end

      it 'render new template' do
        post :create, user: invalid_user
        expect(response).to render_template(:new)
      end

      it 'return http status ok' do
        post :create, user: invalid_user
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      login_with(:user)
    end

    context 'when user exist' do
      let(:user) { create(:user) }

      it 'assigns loaded user to @user' do
        get :show, id: user.id
        expect(assigns[:user]).to eq(user)
      end

      it 'render template show' do
        get :show, id: user.id
        expect(response).to render_template(:show)
      end

      it 'return http status ok' do
        get :show, id: user.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when user does not exist' do
      it 'Raise a ActiveRecord::RecordNotFound error' do
        expect do
          get :show, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'GET #edit' do
    before :each do
      login_with(admin)
    end

    context 'when user exist' do
      let(:user) { create(:user, role: :user) }

      it 'assigns loaded user to @user' do
        get :edit, id: user.id
        expect(assigns[:user]).to eq(user)
      end

      it 'render template edit' do
        get :edit, id: user.id
        expect(response).to render_template(:edit)
      end

      it 'return http status ok' do
        get :edit, id: user.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when user does not exist' do
      it 'Raise a ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      login_with(admin)
    end

    context 'when user exist' do
      context 'with valid params' do
        let(:user) { create(:user, role: :user) }
        let(:attributes) { attributes_for(:user, role: :user) }

        it 'update user attributes' do
          put :update, id: user.id, user: attributes
          user.reload
          expect(user.first_name).to eq(attributes[:first_name])
        end

        it 'redirect to user details (show)' do
          put :update, id: user.id, user: attributes
          expect(response).to redirect_to(user_path(user))
        end

        it 'have http status redirect' do
          put :update, id: user.id, user: attributes
          expect(response).to have_http_status(:redirect)
        end
      end

      context 'with invalid params' do
        let(:user) { create(:user, role: :user) }
        let(:invalid_attributes) { attributes_for(:user, email: nil, role: :user) }

        it 'not update user attributes' do
          expect do
            put :update, id: user.id, user: invalid_attributes
          end.to_not change(user, :email)
        end

        it 'render edit view' do
          put :update, id: user.id, user: invalid_attributes
          expect(response).to render_template(:edit)
        end

        it 'render http status ok' do
          put :update, id: user.id, user: invalid_attributes
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'when user does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      login_with(admin)
    end

    context 'when user exist' do
      let(:user) { create(:user, role: :user) }

      it 'remove user from database' do
        user
        expect do
          delete :destroy, id: user.id
        end.to change(User, :count).by(-1)
      end

      it 'redirect to users listing' do
        delete :destroy, id: user.id
        expect(response).to redirect_to(users_path)
      end
    end

    context 'when user not exist' do
      it 'Raise a ActiveRecord::RecordNotFound error' do
        expect do
          delete :destroy, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end

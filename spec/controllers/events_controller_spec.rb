require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let(:user) { create(:user, role: :user) }
  let(:admin) { create(:user, role: :admin) }

  describe 'GET #index' do
    before :each do
      login_with(user)
    end

    let(:events) { create_list(:event, 5) }

    it 'assign events to @events' do
      get :index
      expect(assigns[:events]).to match_array(events)
    end

    it 'render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'return http status ok' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #new' do
    before :each do
      login_with(admin)
    end

    it 'assigns a new Event to @event' do
      get :new
      expect(assigns[:event]).to be_a_new(Event)
    end

    it 'render template new' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'return http status ok' do
      get :new
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    before :each do
      login_with(admin)
    end

    context 'with valid params' do
      let(:event) { attributes_for(:event) }

      it 'create event' do
        expect do
          post :create, event: event
        end.to change(Event, :count).by(1)
      end

      it 'redirect to event details (show)' do
        post :create, event: event
        expect(response).to redirect_to(event_path(Event.last))
      end

      it 'returns an http status redirect' do
        post :create, event: event
        expect(response).to have_http_status(:redirect)
      end
    end

    context 'with invalid params' do
      let(:invalid_event) { attributes_for(:event, title: nil) }

      it 'do not create event' do
        expect do
          post :create, event: invalid_event
        end.to_not change(Event, :count)
      end

      it 'render new template' do
        post :create, event: invalid_event
        expect(response).to render_template(:new)
      end

      it 'returns an http status ok' do
        post :create, event: invalid_event
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      login_with(user)
    end

    context 'when event exist' do
      let(:event) { create(:event) }

      it 'assigns loaded event to @event' do
        get :show, id: event.id
        expect(assigns[:event]).to eq(event)
      end

      it 'render template show' do
        get :show, id: event.id
        expect(response).to render_template(:show)
      end

      it 'return http status ok' do
        get :show, id: event.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when event does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :show, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'GET #edit' do
    before :each do
      login_with(admin)
    end

    context 'when event exist' do
      let(:event) { create(:event, user_id: admin.id) }

      it 'assigns loaded event to @event' do
        get :edit, id: event.id
        expect(assigns[:event]).to eq(event)
      end

      it 'render template edit' do
        get :edit, id: event.id
        expect(response).to render_template(:edit)
      end

      it 'return http status ok' do
        get :edit, id: event.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when event does not exists' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      login_with(admin)
    end

    context 'when event exist' do
      context 'with valid params' do
        let(:event) { create(:event, user_id: admin.id) }
        let(:attributes) { attributes_for(:event, user_id: admin.id) }

        it 'update event attributes' do
          put :update, id: event.id, event: attributes
          event.reload
          expect(event.title).to eq(attributes[:title])
        end

        it 'redirect to event details (show)' do
          put :update, id: event.id, event: attributes
          expect(response).to redirect_to(event_path(event))
        end

        it 'have http status redirect' do
          put :update, id: event.id, event: attributes
          expect(response).to have_http_status(:redirect)
        end
      end

      context 'with invalid params' do
        let(:event) { create(:event, user_id: admin.id) }
        let(:invalid_attributes) { attributes_for(:event, title: nil) }

        it 'not update event attributes' do
          expect do
            put :update, id: event.id, event: invalid_attributes
          end.to_not change(event, :title)
        end

        it 'render edit view' do
          put :update, id: event.id, event: invalid_attributes
          expect(response).to render_template(:edit)
        end

        it 'have http status ok' do
          put :update, id: event.id, event: invalid_attributes
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'when event not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      login_with(admin)
    end

    context 'when event exist' do
      let(:event) { create(:event, user_id: admin.id) }

      it 'remove event from database' do
        event
        expect do
          delete :destroy, id: event.id
        end.to change(Event, :count).by(-1)
      end

      it 'redirect to events listing' do
        delete :destroy, id: event.id
        expect(response).to redirect_to(events_path)
      end
    end

    context 'when event not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          delete :destroy, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end

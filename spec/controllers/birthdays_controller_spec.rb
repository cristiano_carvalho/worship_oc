require 'rails_helper'

RSpec.describe BirthdaysController, type: :controller do
  let(:user) { build_stubbed(:user, role: :user) }

  describe 'GET #index' do
    before :each do
      login_with(user)
    end

    let(:users) { create_list(:user, 5) }

    it 'assign users to @users' do
      get :index
      expect(assigns[:users]).to match_array(users)
    end

    it 'render index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'return http status ok' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end

end

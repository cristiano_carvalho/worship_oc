require 'rails_helper'

RSpec.describe SongsController, type: :controller do
  let(:user) { create :user, role: :user }
  before :each do
    login_with(user)
  end

  describe 'GET #index' do
    context 'show only songs with current week number' do
      let(:songs) { create_list(:song, 5) }

      it 'assign songs to @songs' do
        get :index
        expect(assigns[:songs]).to match_array(songs)
      end

      it 'render index template' do
        get :index
        expect(response).to render_template(:index)
      end

      it 'return http status ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end

    context 'not show songs out of current week number' do
      let(:songs) { create_list(:song, 5, week_number: Date.current.cweek - 1) }

      it 'not assign songs to @songs' do
        get :index
        expect(assigns[:songs]).to_not match_array(songs)
      end

      it 'render index template' do
        get :index
        expect(response).to render_template(:index)
      end

      it 'return http status ok' do
        get :index
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET #new' do
    it 'assigns a new Song to @song' do
      get :new
      expect(assigns[:song]).to be_a_new(Song)
    end

    it 'render template new' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'return http status ok' do
      get :new
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      let(:song) { attributes_for(:song) }

      it 'create song' do
        expect do
          post :create, song: song
        end.to change(Song, :count).by(1)
      end

      it 'redirect to event details (show)' do
        post :create, song: song
        expect(response).to redirect_to(song_path(Song.last))
      end

      it 'return an http status redirect' do
        post :create, song: song
        expect(response).to have_http_status(:redirect)
      end
    end

    context 'with invalid params' do
      let(:invalid_song) { attributes_for(:song, title: nil) }

      it 'do not create song' do
        expect do
          post :create, song: invalid_song
        end.to_not change(Song, :count)
      end

      it 'render new template' do
        post :create, song: invalid_song
        expect(response).to render_template(:new)
      end

      it 'returns an http status ok' do
        post :create, song: invalid_song
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'GET #show' do
    context 'when song exist' do
      let(:song) { create(:song) }

      it 'assigns loaded song to @song' do
        get :show, id: song.id
        expect(assigns[:song]).to eq(song)
      end

      it 'render template show' do
        get :show, id: song.id
        expect(response).to have_http_status(:ok)
      end

      it 'return http status ok' do
        get :show, id: song.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when song does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :show, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'GET #edit' do
    context 'when song exists' do
      let(:song) { create(:song, user_id: user.id) }

      before :each do
        login_with(user)
      end

      it 'assigns loaded song to @song' do
        get :edit, id: song.id
        expect(assigns[:song]).to eq(song)
      end

      it 'render template edit' do
        get :edit, id: song.id
        expect(response).to render_template(:edit)
      end

      it 'return http status ok' do
        get :edit, id: song.id
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when song does not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'PUT #update' do
    context 'when song exist' do
      context 'with valid params' do
        let(:song) { create(:song, user_id: user.id) }
        let(:attributes) { attributes_for(:song) }

        it 'update song attributes' do
          put :update, id: song.id, song: attributes
          song.reload
          expect(song.title).to eq(attributes[:title])
        end

        it 'redirect to song details (show)' do
          put :update, id: song.id, song: attributes
          expect(response).to redirect_to(song_path(song))
        end

        it 'have http status redirect' do
          put :update, id: song.id, song: attributes
          expect(response).to have_http_status(:redirect)
        end
      end

      context 'with invalid params' do
        let(:song) { create(:song, user_id: user.id) }
        let(:invalid_attributes) { attributes_for(:song, title: nil) }

        it 'not update song attributes' do
          expect do
            put :update, id: song.id, song: invalid_attributes
          end.to_not change(song, :title)
        end

        it 'render edit view' do
          put :update, id: song.id, song: invalid_attributes
          expect(response).to render_template(:edit)
        end

        it 'have http status ok' do
          put :update, id: song.id, song: invalid_attributes
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'when song not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          get :edit, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when song exist' do
      let(:song) { create(:song, user_id: user.id) }

      it 'remove song from database' do
        song
        expect do
          delete :destroy, id: song.id
        end.to change(Song, :count).by(-1)
      end

      it 'redirect to songs listing' do
        delete :destroy, id: song.id
        expect(response).to redirect_to(songs_path)
      end
    end

    context 'when song not exist' do
      it 'Raise an ActiveRecord::RecordNotFound error' do
        expect do
          delete :destroy, id: -9999
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end

require 'rails_helper'

feature 'List songs' do
  scenario 'See a list of existent songs' do
    login_as create(:user), scope: :user

    songs = create_list(:song, 5)

    visit root_path

    click_link 'Louvores Avulsos'

    songs.each do |song|
      expect(page).to have_text(song.title)
    end
  end

  scenario 'see a message when there is no songs already added' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Louvores Avulsos'

    expect(page).to have_text('Ainda não existem louvores registrados.')
  end
end

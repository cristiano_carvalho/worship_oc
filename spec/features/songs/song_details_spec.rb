require 'rails_helper'

feature 'See song details', js: true do
  scenario 'List information about song' do
    login_as create(:user), scope: :user

    song = create(:song)

    visit root_path

    click_link 'Louvores Avulsos'

    within "#show_song_#{song.id}" do
      find(:css, 'i.glyphicon.glyphicon-info-sign').trigger('click')
    end

    expect(page).to have_content(song.title)
    expect(page).to have_content(song.singer)
    expect(page).to have_content(song.lyrics)
    expect(page).to have_content(song.chords)
  end
end

require 'rails_helper'

feature 'Delete song', js: true do
  scenario 'from database' do
    user = create :user

    login_as user

    song = create(:song, user_id: user.id)

    visit root_path
    click_link 'Louvores Avulsos'

    within "#delete_song_#{song.id}" do
      page.accept_confirm do
        find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
      end
    end

    expect(page).to_not have_content(song.title)
  end
end

require 'rails_helper'

feature 'Check week days select box options' do
  scenario 'check if field options combines with defined options in enum week_day' do
    login_as create(:user), scope: :user

    visit root_path

    click_link "Louvores Avulsos"
    click_link "Adicionar Louvor"

    within("#week_days") do
      week_days = ['Domingo',
                   'Segunda-feira',
                   'Terça-Feira',
                   'Quarta-Feira',
                   'Quinta-Feira',
                   'Sexta-Feira',
                   'Sábado'
                  ]

      week_days.each do |day|
        expect(find("option[value=#{day}]").text).to eq(day)
      end
    end
  end
end

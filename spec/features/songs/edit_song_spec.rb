require 'rails_helper'

feature 'Edit song', js: true do
  scenario 'with valid data change song attributes' do
    user = create :user

    login_as user

    song = create(:song, user_id: user.id)

    visit root_path

    click_link 'Louvores Avulsos'

    within "#edit_song_#{song.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_title = Faker::Name.title
    fill_in 'song_title', with: new_title

    click_button 'Salvar'

    expect(page).to have_content(new_title)
  end

  scenario 'with invalid data do not change song attributes' do
    user = create :user

    login_as user

    song = create(:song, user_id: user.id)

    visit root_path

    click_link 'Louvores Avulsos'

    within "#edit_song_#{song.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    fill_in 'song_title', with: ''

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

require 'rails_helper'

feature 'Create song' do
  scenario 'with valid data create song' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Louvores Avulsos'
    click_link 'Adicionar Louvor'

    song = attributes_for(:song)

    fill_in 'song_title', with: song[:title]

    click_button 'Salvar'

    expect(page).to have_content(song[:title])
  end

  scenario 'with invalid data do not create song' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Louvores Avulsos'
    click_link 'Adicionar Louvor'

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

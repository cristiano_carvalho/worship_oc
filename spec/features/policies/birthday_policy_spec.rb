require 'rails_helper'

feature 'BirthdayPolicy' do
  context '#index' do
    scenario 'allows access for an user' do
      user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Aniversários'

      expect(page).to have_text(user.birthday.try(:strftime, '%d/%m/%Y'))
    end

    scenario 'allows access for an admin' do
      admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Aniversários'

      expect(page).to have_text(admin.birthday.try(:strftime, '%d/%m/%Y'))
    end
  end
end

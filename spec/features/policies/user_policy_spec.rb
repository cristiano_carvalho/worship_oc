require 'rails_helper'

feature 'UserPolicy', js: true do
  context '#index' do
    scenario 'allows access for an user' do
      user =  create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      expect(page).to have_text(user.email)
    end

    scenario 'allows access for an admin' do
      admin =  create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      expect(page).to have_text(admin.email)
    end
  end

  context '#show' do
    scenario 'allows access for an user' do
      user =  create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      expect(page).to have_text(user.email)
    end

    scenario 'allows access for an admin' do
      admin =  create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      expect(page).to have_text(admin.email)
    end
  end

  context '#new' do
    scenario 'denies access if not an admin' do
      user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      click_link 'Adicionar Usuário'

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows access for an admin' do
      admin =  create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      click_link 'Adicionar Usuário'

      expect(page).to have_text('Novo Usuário')
    end
  end

  context '#edit' do
    scenario 'prevents user to edit other user attributes' do
      user = create :user, role: :user
      other_user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an user to edit his own profile attributes' do
      user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path user.id

      expect(page).to have_text('John Doe')
    end

    scenario 'allows an admin to edit his own profile attributes' do
      admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path admin.id

      expect(page).to have_text('John Doe')
    end

    scenario 'prevents an admin to edit other admin' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to edit any user' do
      admin = create :user, role: :admin
      user = create :user, role: :user

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path user.id

      expect(page).to have_text('John Doe')
    end
  end

  context '#create' do
    scenario 'prevents create user if not an admin' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Usuários'

      click_link 'Adicionar Usuário'

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to create user' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Usuários'

      click_link 'Adicionar Usuário'

      expect(page).to have_text('Novo Usuário')
    end
  end

  context '#update' do
    scenario 'allows user to update his own account' do
      user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path user.id

      expect(page).to have_text('John Doe')
    end

    scenario 'prevents user to update other user account' do
      user = create :user, role: :user
      other_user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to update his own account' do
      admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path admin.id

      expect(page).to have_text('John Doe')
    end

    scenario 'prevents an admin to update other admin account' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to update any user' do
      admin = create :user, role: :admin
      user = create :user, role: :user

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      fill_in 'user_first_name', with: 'John'
      fill_in 'user_last_name', with: 'Doe'
      click_button 'Salvar'

      visit user_path user.id

      expect(page).to have_text('John Doe')
    end
  end

  context '#destroy' do
    scenario 'prevents user to delete other user' do
      user = create :user, role: :user
      other_user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allow user to delete himself' do
      user = create :user, role: :user

      login_as user

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      click_link 'Excluir conta'

      expect(page).to have_text('Para continuar, efetue login ou registre-se.')
    end

    scenario 'prevents an admin to delete other admin' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{other_admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to delete himself' do
      admin = create :user, role: :admin

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{admin.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      click_link 'Excluir conta'

      expect(page).to have_text('Para continuar, efetue login ou registre-se.')
    end

    scenario 'allows an admin to delete any user' do
      admin = create :user, role: :admin
      user = create :user, role: :user

      login_as admin

      visit root_path

      click_link 'Usuários'

      within "#edit_user_#{user.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      click_link 'Excluir conta'

      expect(page).to have_text('O usuário foi excluído com sucesso.')
    end
  end
end

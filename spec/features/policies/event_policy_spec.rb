require 'rails_helper'

feature 'EventPolicy', js: true do
  context '#index' do
    scenario 'allows access for an user' do
      events = create_list :event, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Eventos'

      events.each do |event|
        expect(page).to have_text(event.title)
      end
    end

    scenario 'allows access for an admin' do
      events = create_list :event, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Eventos'

      events.each do |event|
        expect(page).to have_text(event.title)
      end
    end
  end

  context '#show' do
    scenario 'allows access for an user' do
      events = create_list :event, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Eventos'

      events.each do |event|
        expect(page).to have_text(event.title)
      end
    end

    scenario 'allows access for an admin' do
      events = create_list :event, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Eventos'

      events.each do |event|
        expect(page).to have_text(event.title)
      end
    end
  end

  context '#new' do
    scenario 'denies access if not an admin' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Eventos'

      click_link 'Adicionar Evento'

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows access if an admin' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Eventos'

      click_link 'Adicionar Evento'

      expect(page).to have_text('Novo Evento')
    end
  end

  context '#edit' do
    scenario 'allows an admin to edit his own event' do
      admin = create :user, role: :admin
      event = create :event, user_id: admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#edit_event_#{event.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_event_title = Faker::Name.title
      fill_in 'event_title', with: new_event_title

      click_button 'Salvar'

      visit event_path event.id

      expect(page).to have_text(new_event_title)
    end

    scenario 'prevents an admin to edit other admin event' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin
      event = create :event, user_id: other_admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#edit_event_#{event.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#create' do
    scenario 'prevents an user to create an event' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Eventos'

      click_link 'Adicionar Evento'

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end

    scenario 'allows an admin to create an event' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Eventos'

      click_link 'Adicionar Evento'

      expect(page).to have_text('Novo Evento')
    end
  end

  context '#update' do
    scenario 'allows an admin to edit his own event' do
      admin = create :user, role: :admin
      event = create :event, user_id: admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#edit_event_#{event.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_event_title = Faker::Name.title
      fill_in 'event_title', with: new_event_title

      click_button 'Salvar'

      visit event_path event.id

      expect(page).to have_text(new_event_title)
    end

    scenario 'prevents an admin to update other admin event' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin
      event = create :event, user_id: other_admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#edit_event_#{event.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#destroy' do
    scenario 'allows an admin to delete his own event' do
      admin = create :user, role: :admin
      event = create :event, user_id: admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#delete_event_#{event.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to_not have_text(event.title)
      expect(page).to have_text('O evento foi excluído com sucesso.')
    end

    scenario 'prevents an admin to delete other admin event' do
      admin = create :user, role: :admin
      other_admin = create :user, role: :admin
      event = create :event, user_id: other_admin.id

      login_as admin

      visit root_path

      click_link 'Eventos'

      within "#delete_event_#{event.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end
end

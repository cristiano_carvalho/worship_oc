require 'rails_helper'

feature 'SongPolicy', js: true do
  context '#index' do
    scenario 'allows access for an user' do
      songs = create_list :song, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Louvores Avulsos'

      songs.each do |song|
        expect(page).to have_text(song.title)
      end
    end

    scenario 'allows access for an admin' do
      songs = create_list :song, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Louvores Avulsos'

      songs.each do |song|
        expect(page).to have_text(song.title)
      end
    end
  end

  context '#show' do
    scenario 'allows access for an user' do
      songs = create_list :song, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Louvores Avulsos'

      songs.each do |song|
        expect(page).to have_text(song.title)
      end
    end

    scenario 'allows access for an admin' do
      songs = create_list :song, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Louvores Avulsos'

      songs.each do |song|
        expect(page).to have_text(song.title)
      end
    end
  end

  context '#new' do
    scenario 'allows an user to create a new song' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Louvores Avulsos'

      click_link 'Adicionar Louvor'

      expect(page).to have_text('Novo Louvor')
    end

    scenario 'allows an admin to create a new song' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Louvores Avulsos'

      click_link 'Adicionar Louvor'

      expect(page).to have_text('Novo Louvor')
    end
  end

  context '#edit' do
    scenario 'allows user to edit his own song' do
      user = create :user, role: :user
      song = create :song, user_id: user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#edit_song_#{song.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_song_title = Faker::Name.title
      fill_in 'song_title', with: new_song_title

      click_button 'Salvar'

      visit song_path song.id

      expect(page).to have_text(new_song_title)
    end

    scenario 'prevents user to edit other user song' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      song = create :song, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#edit_song_#{song.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#create' do
    scenario 'allows an user to create a new song' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Louvores Avulsos'

      click_link 'Adicionar Louvor'

      expect(page).to have_text('Novo Louvor')
    end

    scenario 'allows an admin to create a new song' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Louvores Avulsos'

      click_link 'Adicionar Louvor'

      expect(page).to have_text('Novo Louvor')
    end
  end

  context '#update' do
    scenario 'allows user to update his own song' do
      user = create :user, role: :user
      song = create :song, user_id: user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#edit_song_#{song.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_song_title = Faker::Name.title
      fill_in 'song_title', with: new_song_title

      click_button 'Salvar'

      visit song_path song.id

      expect(page).to have_text(new_song_title)
    end

    scenario 'prevents user to update other user song' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      song = create :song, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#edit_song_#{song.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#destroy' do
    scenario 'allows an user to delete his own song' do
      user = create :user, role: :user
      song = create :song, user_id: user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#delete_song_#{song.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to_not have_text(song.title)
      expect(page).to have_text('O louvor foi excluído com sucesso.')
    end

    scenario 'prevents an user to delete other user song' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      song = create :song, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Louvores Avulsos'

      within "#delete_song_#{song.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end
end

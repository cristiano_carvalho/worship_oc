require 'rails_helper'

feature 'HymnPolicy', js: true do
  context '#index' do
    scenario 'allows access for an user' do
      hymns = create_list :hymn, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Harpa Cristã'

      hymns.each do |hymn|
        expect(page).to have_text(hymn.title)
      end
    end

    scenario 'allows access for an admin' do
      hymns = create_list :hymn, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Harpa Cristã'

      hymns.each do |hymn|
        expect(page).to have_text(hymn.title)
      end
    end
  end

  context '#show' do
    scenario 'allows access for an user' do
      hymns = create_list :hymn, 5
      login_as create :user, role: :user

      visit root_path

      click_link 'Harpa Cristã'

      hymns.each do |hymn|
        expect(page).to have_text(hymn.title)
      end
    end

    scenario 'allows access for an admin' do
      hymns = create_list :hymn, 5
      login_as create :user, role: :admin

      visit root_path

      click_link 'Harpa Cristã'

      hymns.each do |hymn|
        expect(page).to have_text(hymn.title)
      end
    end
  end

  context '#new' do
    scenario 'allows an user to create a new hymn' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Harpa Cristã'

      click_link 'Adicionar Hino'

      expect(page).to have_text('Novo Hino')
    end

    scenario 'allows an admin to create a new hymn' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Harpa Cristã'

      click_link 'Adicionar Hino'

      expect(page).to have_text('Novo Hino')
    end
  end

  context '#edit' do
    scenario 'allows user to edit his own hymn' do
      user = create :user, role: :user
      hymn = create :hymn, user_id: user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#edit_hymn_#{hymn.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_hymn_title = Faker::Name.title
      fill_in 'hymn_title', with: new_hymn_title

      click_button 'Salvar'

      visit hymm_path hymn.id

      expect(page).to have_text(new_hymn_title)
    end

    scenario 'prevents user to edit other user hymn' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      hymn = create :hymn, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#edit_hymn_#{hymn.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#create' do
    scenario 'allows an user to create a new hymn' do
      login_as create :user, role: :user

      visit root_path

      click_link 'Harpa Cristã'

      click_link 'Adicionar Hino'

      expect(page).to have_text('Novo Hino')
    end

    scenario 'allows an admin to create a new hymn' do
      login_as create :user, role: :admin

      visit root_path

      click_link 'Harpa Cristã'

      click_link 'Adicionar Hino'

      expect(page).to have_text('Novo Hino')
    end
  end

  context '#update' do
    scenario 'allows user to update his own hymn' do
      user = create :user, role: :user
      hymn = create :hymn, user_id: user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#edit_hymn_#{hymn.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      new_hymn_title = Faker::Name.title
      fill_in 'hymn_title', with: new_hymn_title

      click_button 'Salvar'

      visit hymm_path hymn.id

      expect(page).to have_text(new_hymn_title)
    end

    scenario 'prevents user to update other user hymn' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      hymn = create :hymn, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#edit_hymn_#{hymn.id}" do
        find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end

  context '#destroy' do
    scenario 'allows an user to delete his own hymn' do
      user = create :user, role: :user
      hymn = create :hymn, user_id: user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#delete_hymn_#{hymn.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to_not have_text(hymn.title)
      expect(page).to have_text('O hino foi excluído com sucesso.')
    end

    scenario 'prevents an user to delete other user hymn' do
      user = create :user, role: :user
      other_user = create :user, role: :user
      hymn = create :hymn, user_id: other_user.id

      login_as user

      visit root_path

      click_link 'Harpa Cristã'

      within "#delete_hymn_#{hymn.id}" do
        page.accept_confirm do
          find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
        end
      end

      expect(page).to have_text('Você não tem permissão para realizar esta ação.')
    end
  end
end

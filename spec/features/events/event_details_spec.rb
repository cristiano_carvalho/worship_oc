require 'rails_helper'
require 'date'

feature 'See event details', js: true do
  scenario 'List information about event' do
    login_as create(:user), scope: :user

    event = create(:event)

    visit root_path

    click_link 'Eventos'

    within "#show_event_#{event.id}" do
      find(:css, 'i.glyphicon.glyphicon-info-sign').trigger('click')
    end

    expect(page).to have_content(event.title)
    expect(page).to have_content(event.date.try(:strftime, '%d/%m/%Y'))
    expect(page).to have_content(event.start_time.strftime('%I:%M %p'))
    expect(page).to have_content(event.end_time.strftime('%I:%M %p'))
    expect(page).to have_content(event.address)
    expect(page).to have_content(event.number)
    expect(page).to have_content(event.complement)
    expect(page).to have_content(event.zip_code)
    expect(page).to have_content(event.neighborhood)
    expect(page).to have_content(event.city)
    expect(page).to have_content(event.observations)
  end
end

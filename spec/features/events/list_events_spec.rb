require 'rails_helper'

feature 'List events' do
  scenario 'See a list of existent events' do
    login_as create(:user), scope: :user

    events = create_list(:event, 5)

    visit root_path

    click_link 'Eventos'

    events.each do |event|
      expect(page).to have_text(event.title)
    end
  end

  scenario 'see a message when there is no events already added' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Eventos'

    expect(page).to have_text('Ainda não existem eventos registrados.')
  end
end

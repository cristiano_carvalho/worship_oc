require 'rails_helper'

feature 'Create event' do
  scenario 'with valid data create event' do
    admin = create :user, role: :admin

    login_as admin

    visit root_path

    click_link 'Eventos'
    click_link 'Adicionar Evento'

    event = attributes_for(:event)

    fill_in 'event_title', with: event[:title]

    click_button 'Salvar'

    expect(page).to have_content(event[:title])
  end

  scenario 'with invalid data do not create event' do
    admin = create :user, role: :admin

    login_as admin

    visit root_path

    click_link 'Eventos'
    click_link 'Adicionar Evento'

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

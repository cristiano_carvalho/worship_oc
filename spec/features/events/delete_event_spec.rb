require 'rails_helper'

feature 'Delete event', js: true do
  scenario 'from database' do
    admin = create :user, role: :admin

    login_as admin

    event = create(:event, user_id: admin.id)

    visit root_path
    click_link 'Eventos'

    within "#delete_event_#{event.id}" do
      page.accept_confirm do
        find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
      end
    end

    expect(page).to_not have_content(event.title)
  end
end

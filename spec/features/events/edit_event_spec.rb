require 'rails_helper'

feature 'Edit event', js: true do
  scenario 'with valid data change event attributes' do
    admin = create :user, role: :admin

    login_as admin

    event = create(:event, user_id: admin.id)

    visit root_path

    click_link 'Eventos'

    within "#edit_event_#{event.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_title = Faker::Name.title
    fill_in 'event_title', with: new_title

    click_button 'Salvar'

    expect(page).to have_content(new_title)
  end

  scenario 'with invalid data do not change event attributes' do
    admin = create :user, role: :admin

    login_as admin

    event = create(:event, user_id: admin.id)

    visit root_path

    click_link 'Eventos'

    within "#edit_event_#{event.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    fill_in 'event_title', with: ''

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

require 'rails_helper'

feature 'As admin create user' do
  scenario 'with valid data create user' do
    login_as create(:user, role: :admin), scope: :user

    visit root_path

    click_link 'Usuários'
    click_link 'Adicionar Usuário'

    user = attributes_for(:user)

    fill_in 'user_email', with: user[:email]
    fill_in 'user_password', with: user[:password]
    fill_in 'user_password_confirmation', with: user[:password]

    click_button 'Salvar'

    expect(page).to have_content(user[:email])
    expect(page).to have_text('O usuário foi criado com sucesso.')
  end

  scenario 'with invalid data do not create user' do
    login_as create(:user, role: :admin), scope: :user

    visit root_path

    click_link 'Usuários'
    click_link 'Adicionar Usuário'

    click_button 'Salvar'

    expect(page).to have_content('Email não pode ficar em branco')
    expect(page).to have_content('Password não pode ficar em branco')
  end

  scenario 'don\'t allow user to create another user' do
    login_as create(:user, role: :user), scope: :user

    visit root_path

    click_link 'Usuários'
    click_link 'Adicionar Usuário'

    expect(page).to have_text('Você não tem permissão para realizar esta ação.')
  end
end

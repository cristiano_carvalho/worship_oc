require 'rails_helper'

feature 'Edit user account attributes', js: true do
  scenario 'as admin with valid data change your own attributes' do
    user = create(:user, role: :admin)

    visit root_path

    email = user.email
    password = user.password
    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_email = Faker::Internet.email
    fill_in 'user_email', with: new_email
    click_button 'Salvar'

    expect(page).to have_content(new_email)
    expect(page).to have_text('O usuário foi alterado com sucesso.')
  end

  scenario 'as user with valid data change your own attributes' do
    user = create(:user, role: :user)

    visit root_path

    email = user.email
    password = user.password
    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_email = Faker::Internet.email
    fill_in 'user_email', with: new_email
    click_button 'Salvar'

    expect(page).to have_content(new_email)
    expect(page).to have_text('O usuário foi alterado com sucesso.')
  end

  scenario 'as admin with valid data change someone else attributes' do
    login_as create(:user, role: :admin), scope: :user

    user = create(:user, role: :user)

    visit root_path

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_email = Faker::Internet.email
    fill_in 'user_email', with: new_email
    click_button 'Salvar'

    expect(page).to have_content(new_email)
    expect(page).to have_text('O usuário foi alterado com sucesso.')
  end

  scenario 'as admin with invalid data do not change someone else attributes' do
    login_as create(:user, role: :admin), scope: :user

    user = create(:user, role: :user)

    visit root_path

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    fill_in 'user_email', with: ''

    click_button 'Salvar'

    expect(page).to have_content('Email não pode ficar em branco')
  end
end

def new_session(email, password)
  fill_in 'user_email', with: email
  fill_in 'user_password', with: password
  click_button 'Entrar'
end

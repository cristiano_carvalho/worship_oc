require 'rails_helper'

feature 'List users' do
  scenario 'See a list of existent users' do
    login_as create(:user, role: :user), scope: :user

    users = create_list(:user, 5)

    visit root_path

    click_link 'Usuários'

    users.each do |user|
      expect(page).to have_text(user.email)
    end
  end
end

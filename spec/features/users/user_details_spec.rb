require 'rails_helper'

feature 'See user details', js: true do
  scenario 'List information about user' do
    login_as create(:user, role: :user), scope: :user

    user = create(:user, role: :user)

    visit root_path

    click_link 'Usuários'

    within "#show_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-info-sign').trigger('click')
    end

    expect(page).to have_content(user.first_name)
    expect(page).to have_content(user.last_name)
    expect(page).to have_content(user.birthday.try(:strftime, '%d/%m/%Y'))
    expect(page).to have_content(user.email)
  end
end

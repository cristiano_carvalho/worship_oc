require 'rails'

feature 'Reset password', js: true do
  scenario 'as admin reset your own password' do
    user = create(:user, role: :admin)

    visit root_path

    email = user.email
    password = user.password

    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_password

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_text 'Para continuar, efetue login ou registre-se.'
  end

  scenario 'as user reset your own password' do
    user = create(:user, role: :user)

    visit root_path

    email = user.email
    password = user.password

    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_password

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_text 'Para continuar, efetue login ou registre-se.'
  end

  scenario 'as admin reset someone else password' do
    login_as create(:user, role: :admin), scope: :user
    visit root_path

    user = create(:user, role: :user)

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_password

    expect(page).to have_current_path user_path user.id
    expect(page).to have_text('O usuário foi alterado com sucesso.')
  end
end

def new_session(email, password)
  fill_in 'user_email', with: email
  fill_in 'user_password', with: password
  click_button 'Entrar'
end

def new_password
  fill_in 'user_password', with: 'n3wp455w0rd'
  fill_in 'user_password_confirmation', with: 'n3wp455w0rd'
  click_button 'Redefinir'
end

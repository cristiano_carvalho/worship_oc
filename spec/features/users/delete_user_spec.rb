require 'rails_helper'

feature 'Delete user account', js: true do
  scenario 'as admin delete someone else user account' do
    login_as create(:user, role: :admin), scope: :user
    user = create(:user, role: :user)

    visit root_path
    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    page.accept_confirm do
      click_link 'Excluir conta'
    end

    expect(page).to_not have_content(user.email)
    expect(page).to_not have_content(user.password)
    expect(page).to have_text('O usuário foi excluído com sucesso.')
  end

  scenario 'as admin delete your own account' do
    user = create(:user, role: :admin)

    visit root_path

    email = user.email
    password = user.password
    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    page.accept_confirm do
      click_link 'Excluir conta'
    end

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_text 'Para continuar, efetue login ou registre-se.'
  end

  scenario 'as user delete your own account' do
    user = create(:user, role: :user)

    visit root_path

    email = user.email
    password = user.password
    new_session email, password

    click_link 'Usuários'

    within "#edit_user_#{user.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    page.accept_confirm do
      click_link 'Excluir conta'
    end

    expect(page).to have_current_path new_user_session_path
    expect(page).to have_text 'Para continuar, efetue login ou registre-se.'
  end
end

def new_session(email, password)
  fill_in 'user_email', with: email
  fill_in 'user_password', with: password
  click_button 'Log in'
end

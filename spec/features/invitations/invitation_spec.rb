require 'rails_helper'

feature 'Send invitation' do
  scenario 'as an admin send invitation for an user to join the app' do
    login_as create :user, role: :admin

    visit root_path

    click_link 'Usuários'

    click_link 'Convidar Usuário'

    invited_email = Faker::Internet.email
    fill_in 'user_email', with: invited_email

    click_button 'Enviar um convite'

    expect(page).to have_text("Um email de convite foi enviado para #{invited_email}")

  end

  scenario 'not permit an user to send and invitation' do
    login_as create :user, role: :user

    visit root_path

    click_link 'Usuários'

    click_link 'Convidar Usuário'

    expect(page).to have_text('Apenas o adminstrador pode convidar usuários!')
  end
end

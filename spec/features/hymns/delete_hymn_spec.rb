require 'rails_helper'

feature 'Delete hymn', js: true do
  scenario 'from database' do
    user = create :user

    login_as user

    hymn = create(:hymn, user_id: user.id)

    visit root_path
    click_link 'Harpa Cristã'

    within "#delete_hymn_#{hymn.id}" do
      page.accept_confirm do
        find(:css, 'i.glyphicon.glyphicon-trash').trigger('click')
      end
    end

    expect(page).to_not have_content(hymn.title)
  end
end

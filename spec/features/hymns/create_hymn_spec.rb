require 'rails_helper'

feature 'Create hymn' do
  scenario 'with valid data create hymn' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Harpa Cristã'
    click_link 'Adicionar Hino'

    hymn = attributes_for(:hymn)

    fill_in 'hymn_title', with: hymn[:title]

    click_button 'Salvar'

    expect(page).to have_content(hymn[:title])
  end

  scenario 'with invalid data do not create hymn' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Harpa Cristã'
    click_link 'Adicionar Hino'

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

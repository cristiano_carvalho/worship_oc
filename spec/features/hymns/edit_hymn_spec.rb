require 'rails_helper'

feature 'Edit hymn', js: true do
  scenario 'with valid data change hymn attributes' do
    user = create :user

    login_as user

    hymn = create(:hymn, user_id: user.id)

    visit root_path

    click_link 'Harpa Cristã'

    within "#edit_hymn_#{hymn.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    new_title = Faker::Name.title
    fill_in 'hymn_title', with: new_title

    click_button 'Salvar'

    expect(page).to have_content(new_title)
  end

  scenario 'with invalid data do not change hymn attributes' do
    user = create :user

    login_as user

    hymn = create(:hymn, user_id: user.id)

    visit root_path

    click_link 'Harpa Cristã'

    within "#edit_hymn_#{hymn.id}" do
      find(:css, 'i.glyphicon.glyphicon-edit').trigger('click')
    end

    fill_in 'hymn_title', with: ''

    click_button 'Salvar'

    expect(page).to have_content('Title não pode ficar em branco')
  end
end

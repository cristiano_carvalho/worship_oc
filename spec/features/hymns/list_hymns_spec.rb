require 'rails_helper'

feature 'List hymns' do
  scenario 'See a list of existent hymns' do
    login_as create(:user), scope: :user

    hymns = create_list(:hymn, 5)

    visit root_path

    click_link 'Harpa Cristã'

    hymns.each do |hymn|
      expect(page).to have_text(hymn.title)
    end
  end

  scenario 'see a message when there is no hymns already added' do
    login_as create(:user), scope: :user

    visit root_path

    click_link 'Harpa Cristã'

    expect(page).to have_text('Ainda não existem hinos registrados.')
  end
end

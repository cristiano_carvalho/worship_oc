require 'rails_helper'

feature 'See hymn details', js: true do
  scenario 'List information about hymn' do
    login_as create(:user), scope: :user

    hymn = create(:hymn)

    visit root_path

    click_link 'Harpa Cristã'

    within "#show_hymn_#{hymn.id}" do
      find(:css, 'i.glyphicon.glyphicon-info-sign').trigger('click')
    end

    expect(page).to have_content(hymn.title)
    expect(page).to have_content(hymn.number)
    expect(page).to have_content(hymn.lyrics)
    expect(page).to have_content(hymn.chords)
  end
end

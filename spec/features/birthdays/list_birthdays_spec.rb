require 'rails_helper'

feature 'List users birthdays' do
  scenario 'See a list of birthdays' do
    login_as create(:user, role: :user), scope: :user

    users = create_list(:user, 5)

    visit root_path

    click_link 'Aniversários'

    users.each do |user|
      expect(page).to have_text(user.birthday.try(:strftime, '%d/%m/%Y'))
    end
  end
end

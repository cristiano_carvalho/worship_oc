require 'rails_helper'

RSpec.describe SongPolicy do
  let(:current_user) { create(:user, role: :user) }
  let(:other_user)   { create(:user, role: :user) }
  let(:admin)        { create(:user, role: :admin) }

  let(:song)         { create(:song, user_id: current_user.id) }
  let(:other_song)   { create(:song, user_id: other_user.id) }


  subject { described_class }

  permissions :index? do
    it 'allow access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allow access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :show? do
    it 'allow access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allow access for and admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :new? do
    it 'allows an user to create a new song' do
      expect(subject).to permit(current_user)
    end

    it 'allows an admin to create a new song' do
      expect(subject).to permit(admin)
    end
  end

  permissions :edit? do
    it 'allows user to edit his own song' do
      expect(subject).to permit(current_user, song)
    end

    it 'prevents user to edit other user song' do
      expect(subject).to_not permit(current_user, other_song)
    end
  end

  permissions :create? do
    it 'allows an user to create song' do
      expect(subject).to permit(current_user)
    end

    it 'allows and admin to create song' do
      expect(subject).to permit(admin)
    end
  end

  permissions :update? do
    it 'allows an user to update his own song' do
      expect(subject).to permit(current_user, song)
    end

    it 'prevents an user to update other user song' do
      expect(subject).to_not permit(current_user, other_song)
    end
  end

  permissions :destroy? do
    it 'allows an user to delete his own song' do
      expect(subject).to permit(current_user, song)
    end

    it 'prevents an user to delete other user song' do
      expect(subject).to_not permit(current_user, other_song)
    end
  end
end


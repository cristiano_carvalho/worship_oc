require 'rails_helper'

RSpec.describe EventPolicy do
  let(:current_user)  { create(:user, role: :user) }
  let(:admin)         { create(:user, role: :admin) }
  let(:other_admin)   { create(:user, role: :admin) }

  let(:event)         { create(:event, user_id: admin.id) }
  let(:other_event)   { create(:event, user_id: other_admin.id) }

  subject { described_class }

  permissions :index? do
    it 'allows access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allows access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :show? do
    it 'allows access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allows access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :new? do
    it 'denies access if user is not an admin' do
      expect(subject).to_not permit(current_user)
    end

    it 'allows access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :edit? do
    it 'allows an admin to edit his own event' do
      expect(subject).to permit(admin, event)
    end

    it 'prevents an admin to edit other admin event' do
      expect(subject).to_not permit(admin, other_event)
    end
  end

  permissions :create? do
    it 'prevents an user to create an event' do
      expect(subject).to_not permit(current_user)
    end

    it 'allows an admin to create an event' do
      expect(subject).to permit(admin)
    end
  end

  permissions :update? do
    it 'allows an admin to edit his own event' do
      expect(subject).to permit(admin, event)
    end

    it 'prevents an admin to edit other users admin' do
      expect(subject).to_not permit(admin, other_event)
    end
  end

  permissions :destroy? do
    it 'allows an admin to delete his own event' do
      expect(subject).to permit(admin, event)
    end

    it 'prevents an admin to delete other admin event' do
      expect(subject).to_not permit(admin, other_event)
    end
  end
end

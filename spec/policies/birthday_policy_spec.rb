require 'rails_helper'

RSpec.describe BirthdayPolicy do
  let(:current_user) { build_stubbed(:user, role: :user) }
  let(:admin)        { build_stubbed(:user, role: :admin) }

  subject { described_class }

  permissions :index? do
    it 'allow access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allow access for an admin' do
      expect(subject).to permit(admin)
    end
  end

end

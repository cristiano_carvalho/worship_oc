require 'rails_helper'

RSpec.describe UserPolicy do
  let(:current_user) { build_stubbed(:user, role: :user) }
  let(:other_user)   { build_stubbed(:user, role: :user) }
  let(:admin)        { build_stubbed(:user, role: :admin) }
  let(:other_admin)  { build_stubbed(:user, role: :admin) }

  subject { described_class }

  permissions :index? do
    it 'allows access for an user' do
      expect(UserPolicy).to permit(current_user)
    end

    it 'allows access for an admin' do
      expect(UserPolicy).to permit(admin)
    end
  end

  permissions :show? do
    it 'allows access for an user' do
      expect(UserPolicy).to permit(current_user)
    end

    it 'allows access for and admin' do
      expect(UserPolicy).to permit(admin)
    end
  end

  permissions :new? do
    it 'denies access if not an admin' do
      expect(UserPolicy).to_not permit(current_user)
    end

    it 'allows access for an admin' do
      expect(UserPolicy).to permit(admin)
    end
  end

  permissions :edit? do
    it 'prevents user to edit other user attributes' do
      expect(subject).to_not permit(current_user, other_user)
    end

    it 'allows user to edit his own profile attributes' do
      expect(subject).to permit(current_user, current_user)
    end

    it 'allows an admin to edit his own user profile' do
      expect(subject).to permit(admin, admin)
    end

    it 'prevents an admin to edit other admin' do
      expect(subject).to_not permit(admin, other_admin)
    end

    it 'allows an admin to edit any user' do
      expect(subject).to permit(admin, other_user)
    end
  end

  permissions :create? do
    it 'prevents create user if not an admin' do
      expect(subject).to_not permit(current_user)
    end

    it 'allows and admin to create user' do
      expect(subject).to permit(admin)
    end
  end

  permissions :update? do
    it 'allows user to update his own account' do
      expect(subject).to permit(current_user, current_user)
    end

    it 'prevents user to update other user account' do
      expect(subject).to_not permit(current_user, other_user)
    end

    it 'allows an admin to update his own account' do
      expect(subject).to permit(admin, admin)
    end

    it 'prevents and admin to update other admin account' do
      expect(subject).to_not permit(admin, other_admin)
    end

    it 'allows an admin to edit any user' do
      expect(subject).to permit(admin, other_user)
    end
  end

  permissions :destroy? do
    it 'prevents user to delete other user' do
      expect(subject).to_not permit(current_user, other_user)
    end

    it 'allow user to delete himself' do
      expect(subject).to permit(current_user, current_user)
    end

    it 'prevents an admin to delete other admin' do
      expect(subject).to_not permit(admin, other_admin)
    end

    it 'allows an admin to delete himself' do
      expect(subject).to permit(admin, admin)
    end

    it 'allows an admin to delete any user' do
      expect(subject).to permit(admin, other_user)
    end
  end
end

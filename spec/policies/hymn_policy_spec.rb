require 'rails_helper'

RSpec.describe HymnPolicy do
  let(:current_user) { create(:user, role: :user) }
  let(:other_user)   { create(:user, role: :user) }
  let(:admin)        { create(:user, role: :admin) }

  let(:hymn)         { create(:hymn, user_id: current_user.id) }
  let(:other_hymn)   { create(:hymn, user_id: other_user.id) }


  subject { described_class }

  permissions :index? do
    it 'allow access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allow access for an admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :show? do
    it 'allow access for an user' do
      expect(subject).to permit(current_user)
    end

    it 'allow access for and admin' do
      expect(subject).to permit(admin)
    end
  end

  permissions :new? do
    it 'allows an user to create a new hymn' do
      expect(subject).to permit(current_user)
    end

    it 'allows an admin to create a new hymn' do
      expect(subject).to permit(admin)
    end
  end

  permissions :edit? do
    it 'allows user to edit his own hymn' do
      expect(subject).to permit(current_user, hymn)
    end

    it 'prevents user to edit other user hymn' do
      expect(subject).to_not permit(current_user, other_hymn)
    end
  end

  permissions :create? do
    it 'allows an user to create hymn' do
      expect(subject).to permit(current_user)
    end

    it 'allows and admin to create hymn' do
      expect(subject).to permit(admin)
    end
  end

  permissions :update? do
    it 'allows an user to update his own hymn' do
      expect(subject).to permit(current_user, hymn)
    end

    it 'prevents an user to update other user hymn' do
      expect(subject).to_not permit(current_user, other_hymn)
    end
  end

  permissions :destroy? do
    it 'allows an user to delete his own hymn' do
      expect(subject).to permit(current_user, hymn)
    end

    it 'prevents an user to delete other user hymn' do
      expect(subject).to_not permit(current_user, other_hymn)
    end
  end
end


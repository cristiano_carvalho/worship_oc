require 'rails_helper'

describe 'Api::Songs', type: :request do
  let(:token) { create(:api_key) }

  describe "GET 'index'" do
    let(:songs) { create_list(:song, 5) }

    it 'gives a success JSON response' do
      get api_v1_songs_path + "?access_token=#{token.access_token}"
      expect(response).to be_success
    end

    it 'renders the JSON stories' do
      get api_v1_songs_path + "?access_token=#{token.access_token}"
      expect(response).to match_json_schema(:song_schema)
    end
  end

  describe "GET 'show'" do
    let(:song) { create(:song) }

    it 'gives a success JSON response' do
      get api_v1_song_path(song.id) + "?access_token=#{token.access_token}"
      expect(response).to be_success
    end

    it 'renders the JSON story' do
      get api_v1_song_path(song.id) + "?access_token=#{token.access_token}"
      expect(json['title']).to match_json_schema(:song_schema)
    end
  end

  describe "POST 'create'" do
    let(:song_params) { attributes_for(:song) }

    it 'persists a new Song record' do
      expect do
        post api_v1_songs_path + "?access_token=#{token.access_token}", song: song_params
      end.to change(Song, :count).by(1)
    end

    it 'returns a serialized version of the new Song record' do
      post api_v1_songs_path, song: song_params
      expect(response).to match_json_schema(:song_schema)
    end
  end

  describe "POST 'update'" do
    let(:song) { create(:song) }

    it 'updates the song record and return the updated song record' do
      patch api_v1_song_path(song.id) + "?access_token=#{token.access_token}", song: { title: 'New Title' }
      expect(json['title']).to eq('New Title')
      expect(response).to match_json_schema(:song_schema)
    end
  end

  describe "DELETE 'destroy'" do
    let!(:song) { create(:song) }

    it 'destroys the song record' do
      expect do
        delete api_v1_song_path(song.id) + "?access_token=#{token.access_token}"
      end.to change(Song, :count).by(-1)
    end

    it 'returns the destroyed song record' do
      delete api_v1_song_path(song.id) + "?access_token=#{token.access_token}"
      expect(response).to match_json_schema(:song_schema)
    end
  end
end

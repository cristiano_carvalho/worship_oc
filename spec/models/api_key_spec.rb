require 'rails_helper'

RSpec.describe ApiKey, type: :model do
  subject { create(:api_key) }
  context 'acess token' do
    it { is_expected.to validate_uniqueness_of(:access_token) }
  end
end

require 'rails_helper'

RSpec.describe User, type: :model do
  it do
    should define_enum_for(:role).with([:user, :admin])
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:password) }
  end

  context 'relations' do
    it { is_expected.to have_many(:songs) }
    it { is_expected.to have_many(:events) }
  end
end

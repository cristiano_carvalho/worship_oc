require 'rails_helper'

RSpec.describe Event, type: :model do
  subject { create(:event) }

  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
  end

  context 'relations' do
    it { is_expected.to belong_to(:user) }
  end
end

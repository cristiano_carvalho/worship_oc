require 'rails_helper'

RSpec.describe Hymn, type: :model do
  subject { create(:hymn) }

  it do
    should define_enum_for(:week_day).with({'Domingo':       0,
                                            'Segunda-feira': 1,
                                            'Terça-Feira':   2,
                                            'Quarta-Feira':  3,
                                            'Quinta-Feira':  4,
                                            'Sexta-Feira':   5,
                                            'Sábado':        6
                                        })
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
  end

  context 'relations' do
    it { is_expected.to belong_to(:user) }
  end
end

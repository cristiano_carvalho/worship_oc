require 'rails_helper'

RSpec.describe BirthdaysController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/birthdays').to route_to('birthdays#index')
    end
  end
end

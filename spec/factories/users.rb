password = '53cr3tp455w0rd'

FactoryGirl.define do
  factory :user, class: 'User' do
    first_name            { Faker::Name.first_name }
    last_name             { Faker::Name.last_name }
    birthday              { Faker::Date.between(20.years.ago, Date.today)}
    email                 { Faker::Internet.email }
    password              { password }
    password_confirmation { password }
  end
end

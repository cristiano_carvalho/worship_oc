week_days = ['Domingo',
             'Segunda-feira',
             'Terça-Feira',
             'Quarta-Feira',
             'Quinta-Feira',
             'Sexta-Feira',
             'Sábado'
            ]

FactoryGirl.define do
  factory :song, class: 'Song' do
    title       { Faker::Name.title }
    singer      { Faker::Name.name }
    lyrics      { Faker::Internet.url }
    chords      { Faker::Internet.url }
    week_number { Date.current.cweek }
    week_day    { week_days.sample }
  end
end

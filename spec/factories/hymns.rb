week_days = ['Domingo',
             'Segunda-feira',
             'Terça-Feira',
             'Quarta-Feira',
             'Quinta-Feira',
             'Sexta-Feira',
             'Sábado'
            ]

number = Faker::Number.number(3)

FactoryGirl.define do
  factory :hymn, class: 'Hymn' do
    title       { Faker::Name.title }
    number      { number }
    lyrics      { "http://www.harpacrista.org/hino/#{number}" }
    chords      { Faker::Internet.url }
    week_day    { week_days.sample }
    user        nil
    week_number { Date.current.cweek }
  end
end

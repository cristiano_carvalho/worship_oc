FactoryGirl.define do
  factory :event, class: 'Event' do
    title        { Faker::Name.title }
    date         { Faker::Date.forward(30) }
    start_time   { '18:00' }
    end_time     { '20:30' }
    address      { Faker::Address.street_name }
    number       { Faker::Address.building_number }
    complement   { Faker::Lorem.sentence }
    zip_code     { Faker::Address.zip_code }
    neighborhood { Faker::Name.title }
    city         { Faker::Address.city }
    observations { Faker::Lorem.paragraph }
  end
end

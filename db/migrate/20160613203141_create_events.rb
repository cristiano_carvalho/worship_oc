class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.datetime :date
      t.time :start_time, default: '18:00'
      t.time :end_time, default: '20:30'
      t.string :address
      t.string :number
      t.string :complement
      t.string :zip_code
      t.string :neighborhood
      t.string :city
      t.text :observations
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

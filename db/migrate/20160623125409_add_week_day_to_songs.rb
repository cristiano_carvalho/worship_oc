class AddWeekDayToSongs < ActiveRecord::Migration
  def change
    add_column :songs, :week_day, :integer, default: 0
  end
end

class AddWeekNumberToHymn < ActiveRecord::Migration
  def change
    add_column :hymns, :week_number, :integer
  end
end

class CreateHymns < ActiveRecord::Migration
  def change
    create_table :hymns do |t|
      t.string :title
      t.integer :number
      t.string :lyrics
      t.string :chords
      t.integer :week_day
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

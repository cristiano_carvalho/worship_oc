# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.create(
  [
    {
      first_name: 'Michelson',
      last_name: "Andrade",
      email: "michelson@worshipoc.com",
      password: "1q2w3e4r",
      password_confirmation: "1q2w3e4r",
      role: :admin
    },
    {
      first_name: 'Cristiano',
      last_name: "Carvalho",
      email: "cristiano@worshipoc.com",
      password: "1q2w3e4r",
      password_confirmation: "1q2w3e4r",
      role: :admin
    }
  ]
)

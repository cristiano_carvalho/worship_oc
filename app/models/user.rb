class User < ActiveRecord::Base
  # imports
  include DeviseInvitable::Inviter

  # relations
  has_many :songs, dependent: :destroy
  has_many :events, dependent: :destroy

  # filter methods
  after_initialize :set_default_role, if: :new_record?
  after_create :send_admin_mail

  # validations
  has_attached_file :image, styles: {
    small: "64x64",
    med: "100x100",
    large: "200x200"
  }, default_url: "med/missing.png"

  validates_attachment_content_type :image, content_type: [
    "image/jpg",
    "image/jpeg",
    "image/png",
    "image/gif"
  ]

  # enumerators
  enum role: [:user, :admin]

  # methods
  def set_default_role
    self.role ||= :user
  end

  def to_s
    [first_name, last_name].join(' ')
  end

  def send_admin_mail
    UserMailer.welcome_email(self).deliver_now
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :invitable,
         :database_authenticatable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable
end

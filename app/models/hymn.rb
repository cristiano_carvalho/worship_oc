class Hymn < ActiveRecord::Base
  # relations
  belongs_to :user

  # validations
  validates :title, presence: true

  # enumerators
  enum week_day: {
      'Domingo' =>       0,
      'Segunda-feira' => 1,
      'Terça-Feira' =>   2,
      'Quarta-Feira' =>  3,
      'Quinta-Feira' =>  4,
      'Sexta-Feira' =>   5,
      'Sábado' =>        6
  }

  # filter methods
  before_create do
    self.week_number = Date.current.cweek if week_number.blank?
  end
end

class SongsController < ApplicationController
  before_action :set_song, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @page_title = "Louvores Avulsos"
    @songs = Song.where(week_number: Date.current.cweek)
    authorize @songs
  end

  def show
    @page_title = @song.title
    authorize @song
  end

  def new
    @page_title = "Adicionar louvor"
    @song = Song.new
    authorize @song
  end

  def edit
    @page_title = @song.title
    authorize @song
  end

  def create
    @song = Song.new(song_params)
    @song.user_id = current_user.try(:id)
    authorize @song
    if @song.save
      redirect_to @song, notice: 'O louvor foi adicionado com sucesso.'
    else
      render :new
    end
  end

  def update
    authorize @song
    if @song.update(song_params)
      redirect_to @song, notice: 'O louvor foi alterado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    authorize @song
    @song.destroy
    redirect_to songs_url, notice: 'O louvor foi excluído com sucesso.'
  end

  private

  def set_song
    @song = Song.find(params[:id])
  end

  def song_params
    params.require(:song).permit(
      :title,
      :singer,
      :lyrics,
      :chords,
      :week_number,
      :week_day,
      :user_id
    )
  end
end

class DashboardController < ApplicationController
  def index
    @page_title = "Painel Administrativo"
    # weekly repertoire
    @songs = Song.where(week_number: Date.current.cweek)
    @hymns = Hymn.where(week_number: Date.current.cweek)

    # birthdays
    @birthdays = User.where("extract(month from birthday)=?", Time.zone.now.month).order(:birthday)

    # events
    @events = Event.where("extract(month from date)=?", Time.zone.now.month).order(:date)
  end
end

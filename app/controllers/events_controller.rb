class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @page_title = "Eventos"
    @events = Event.all
    authorize @events
  end

  def show
    @page_title = @event.title
    authorize @event
  end

  def new
    @page_title = "Adicionar evento"
    @event = Event.new
    authorize @event
  end

  def edit
    @page_title = @event.title
    authorize @event
  end

  def create
    @event = Event.new(event_params)
    @event.user_id = current_user.try(:id)
    authorize @event
    if @event.save
      redirect_to @event, notice: 'O evento foi criado com sucesso.'
    else
      render :new
    end
  end

  def update
    authorize @event
    if @event.update(event_params)
      redirect_to @event, notice: 'O evento foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    authorize @event
    @event.destroy
    redirect_to events_url, notice: 'O evento foi excluído com sucesso.'
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(
      :title,
      :date,
      :start_time,
      :end_time,
      :address,
      :number,
      :complement,
      :zip_code,
      :neighborhood,
      :city,
      :observations,
      :user_id
    )
  end
end

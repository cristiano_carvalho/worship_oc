module Api
  module V1
    class SongsController < ApplicationController
      respond_to :json
      before_action :restrict_access

      def index
        respond_with(Song.all)
      end

      def show
        respond_with(Song.find(params[:id]))
      end

      def create
        @song = Song.new(song_params)
        if @song.save
          respond_to do |format|
            format.json { render json: @song }
          end
        end
      end

      def update
        @song = Song.find(params[:id])
        if @song.update(song_params)
          respond_to do |format|
            format.json { render json: @song }
          end
        end
      end

      def destroy
        respond_with Song.destroy(params[:id])
      end

      private

      def restrict_access
        api_key = ApiKey.find_by_access_token(params[:access_token])
        head :unauthorized unless api_key
      end

      def song_params
        params.require(:song).permit(:title,
          :singer, :lyrics, :chords, :week_number, :week_day, :user_id)
      end
    end
  end
end

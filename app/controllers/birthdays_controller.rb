class BirthdaysController < ApplicationController
  before_action :authenticate_user!

  def index
    @page_title = "Aniversários"
    @users = User.all
    authorize @users
  end
end

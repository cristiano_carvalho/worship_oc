class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  after_action :verify_authorized
  before_action :authenticate_user!

  def index
    @page_title = 'Usuários'
    @users = User.all
    authorize @users
  end

  def show
    @page_title = @user.first_name
    authorize @user
  end

  def new
    @page_title = 'Adicionar usuário'
    @user = User.new
    authorize User
  end

  def edit
    @page_title = @user.first_name
    authorize @user
  end

  def create
    @user = User.new(user_params)
    authorize @user
    if @user.save
      redirect_to @user, notice: 'O usuário foi criado com sucesso.'
    else
      render action: 'new'
    end
  end

  def update
    # avoid password validation while editing user
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    authorize @user

    if @user.update_attributes(user_params)
      redirect_to @user, notice: 'O usuário foi alterado com sucesso.'
    else
      render action: 'edit'
    end
  end

  def destroy
    authorize @user
    @user.destroy
    redirect_to users_url, notice: 'O usuário foi excluído com sucesso.'
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :birthday,
      :email,
      :password,
      :password_confirmation,
      :role,
      :image
    )
  end

end

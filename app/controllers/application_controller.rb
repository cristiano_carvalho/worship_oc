class ApplicationController < ActionController::Base
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :configure_permitted_parameters, if: :devise_controller?

  include Pundit

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def user_not_authorized
    flash[:alert] = 'Você não tem permissão para realizar esta ação.'
    redirect_to(request.referer || root_path)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [
      :first_name,
      :last_name,
      :birthday,
      :email,
      :password,
      :password_confirmation,
      :role
    ])
  end

  def authenticate_inviter!
    unless current_user.admin?
      redirect_to root_url, alert: "Apenas o adminstrador pode convidar usuários!"
    end
    super
  end
end

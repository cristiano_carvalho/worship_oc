class HymnsController < ApplicationController
  before_action :set_hymn, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @page_title = "Harpa Cristã"
    @hymns = Hymn.where(week_number: Date.current.cweek)
    authorize @hymns
  end

  def show
    @page_title = @hymn.title
    authorize @hymn
  end

  def new
    @page_title = "Novo Hino"
    @hymn = Hymn.new
    authorize @hymn
  end

  def edit
    @page_title = @hymn.title
    authorize @hymn
  end

  def create
    @hymn = Hymn.new(hymn_params)
    @hymn.user_id = current_user.try(:id)
    authorize @hymn
    if @hymn.save
      redirect_to @hymn, notice: 'O hino foi adicionado com sucesso.'
    else
      render :new
    end
  end

  def update
    authorize @hymn
    if @hymn.update(hymn_params)
      redirect_to @hymn, notice: 'O hino foi alterado com sucesso.'
    else
      render :edit
    end
  end

  def destroy
    authorize @hymn
    @hymn.destroy
    redirect_to hymns_url, notice: 'O hino foi excluído com sucesso.'
  end

  private
     def set_hymn
      @hymn = Hymn.find(params[:id])
    end

    def hymn_params
      params.require(:hymn).permit(
        :title,
        :number,
        :lyrics,
        :chords,
        :week_number,
        :week_day,
        :user_id
      )
    end
end

class ApplicationMailer < ActionMailer::Base
  default from: 'naoresponda@adoracaooc.com'
  layout 'mailer'
end

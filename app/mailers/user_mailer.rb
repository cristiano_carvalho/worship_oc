class UserMailer < ApplicationMailer
  default from: 'naoresponda@adoracaooc.com'
  include UserHelper
  default to: 'cristiano.codelab@gmail.com'

  def welcome_email(user)
    @full_name = full_name user
    @url = 'https://worship-oc.herokuapp.com'
    mail(to: user.email, subject: 'Bem vindo ao Adoração O.C.!')
  end
end

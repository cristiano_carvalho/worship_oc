module UserHelper
  def full_name(user)
    [user.first_name, user.last_name].join(' ')
  end

  def last_login(user)
    if user.last_sign_in_at.blank?
      return 'Never logged in'
    else
      return time_ago_in_words(@user.last_sign_in_at.to_datetime) + ' atrás'
    end
  end
end

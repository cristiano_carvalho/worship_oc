module ApplicationHelper
  def app_title
    'Worship O.C.'
  end

  def page_title
    if @page_title.blank?
      app_title
    else
      [app_title, @page_title].join(' - ')
    end
  end

  def role_description record
    if record == 'admin'
      'Administrador'
    else
      'Usuário'
    end
  end

  def nav_link text, path
    options = current_page?(path) ? { class: "active" } : {}
    content_tag :li, options do
      link_to text, path
    end
  end

  def user_info
    if current_user.first_name.blank?
      current_user.email
    else
      user_name current_user
    end
  end

  def user_name user
    [user.first_name, user.last_name].join ' '
  end

  def date_format date
    date.try :strftime, '%d/%m/%Y'
  end

  def time_format time
    time.strftime '%I:%M %p'
  end

  def extern_link record
    if record.empty?
      record = 'Link não adicionado'
    else
      link_to record.to_s, record, target: '_blank'
    end
  end

  def age dob
    today = Date.today
    age = today.year - dob.year
    if dob.strftime("%m%d").to_i > today.strftime("%m%d").to_i
      age - 1
    end
    age
  end
end

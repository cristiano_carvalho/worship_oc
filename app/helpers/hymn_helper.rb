module HymnHelper
  def hymn_lyrics record
    record =  "http://www.harpacrista.org/hino/#{@hymn.number}"
    link_to record.to_s, record, target: '_blank'
  end

  def day_label record
    case record
    when 'Domingo'
      'success'
    when 'Quinta-Feira'
      'primary'
    else
      'default'
    end
  end

end

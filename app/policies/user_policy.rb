class UserPolicy < ApplicationPolicy
  attr_reader :current_user, :model
  include PoliciesHelpers

  def initialize(current_user, model)
    @current_user = current_user
    @user = model
  end

  def index?
    is_authenticated?
  end

  def birthdays?
    is_authenticated?
  end

  def show?
    is_authenticated?
  end

  def new?
    is_admin?
  end

  def edit?
    is_admin? && is_user? || is_owner?
  end

  def create?
    is_admin?
  end

  def update?
    is_admin? && is_user? || is_owner?
  end

  def destroy?
    is_admin? && is_user? || is_owner?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end

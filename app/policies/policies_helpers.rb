module PoliciesHelpers
  def is_owner?
    @current_user == @user
  end

  def is_authenticated?
    @current_user = current_user
  end

  def is_user?
    @user.role == 'user'
  end

  def is_admin?
    @current_user.admin?
  end

  def is_song_author?
    @current_user.try(:id) == @song.try(:user_id)
  end

  def is_hymn_author?
    @current_user.try(:id) == @hymn.try(:user_id)
  end

  def is_event_author?
    @current_user.try(:id) == @event.try(:user_id)
  end
end

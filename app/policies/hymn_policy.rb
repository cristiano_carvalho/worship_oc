class HymnPolicy < ApplicationPolicy
  attr_reader :current_user, :model
  include PoliciesHelpers

  def initialize(current_user, model)
    @current_user = current_user
    @hymn = model
  end

  def index?
    is_authenticated?
  end

  def show?
    is_authenticated?
  end

  def new?
    is_authenticated?
  end

  def edit?
    is_hymn_author?
  end

  def create?
    is_authenticated?
  end

  def update?
    is_hymn_author?
  end

  def destroy?
    is_hymn_author?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end

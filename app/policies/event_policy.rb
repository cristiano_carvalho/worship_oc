class EventPolicy < ApplicationPolicy
  attr_reader :current_user, :model
  include PoliciesHelpers

  def initialize(current_user, model)
    @current_user = current_user
    @event = model
  end

  def index?
    is_authenticated?
  end

  def show?
    is_authenticated?
  end

  def new?
    is_admin?
  end

  def edit?
    is_event_author?
  end

  def create?
    is_admin?
  end

  def update?
    is_event_author?
  end

  def destroy?
    is_event_author?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
